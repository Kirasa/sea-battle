package battle;

import java.util.Random;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Kira-sa
 * Date: 08.10.13
 * Time: 23:46
 * To change this template use File | Settings | File Templates.
 */

public class battle {
    private Scanner in = new Scanner(System.in);
    private Random generator = new Random();
    int EdgeLen = 10; //  размер поля

    private int mapAi[][] = new int[EdgeLen][EdgeLen];
    private int mapMy[][] = new int[EdgeLen][EdgeLen];
    private int minMaxMap[][] = new int[EdgeLen][EdgeLen];
    private int draftMap[][] = new int[EdgeLen][EdgeLen];

    private int countMyShips = 20;
    private int countAiShips = 20;

    // заполнение массивов/карт начальными значениями
    public void fillMap() {

        for( int i = 0; i < EdgeLen; i++ ){
            for( int j = 0; j < EdgeLen; j++ ) {
                mapAi[i][j] = 0;
                mapMy[i][j] = 0;
            }
        }

        for( int i = 0; i < EdgeLen; i++ ){
            for( int j = 0; j < EdgeLen; j++ ) {
                minMaxMap[i][j] = 1;
                draftMap[i][j] = 1;
            }
        }
    }

    public boolean ship_is_good(int size, boolean is_horiz,int row_top, int col_left,int [][]map)
    {
        if(is_horiz){
            for(int i =   Math.max(0, row_top - 1); i <=  Math.min(EdgeLen - 1,row_top + 1); ++i){
                for(int j = Math.max(0, col_left - 1); j <=  Math.min(EdgeLen - 1,  col_left + size); ++j)
                    if(map[i][j] == 1) return false;
            }
            return  true;
        }
        else//вертикальный
        {
            for(int i =   Math.max(0, row_top - 1);i <=  Math.min(EdgeLen - 1, row_top + size); ++i){
                for(int j =   Math.max(0, col_left - 1); j <=  Math.min(EdgeLen - 1, col_left + 1);++j)
                    if(map[i][j] == 1) return false;
            }
            return  true;
        }
    }

    public void  set_ship_with_size(int  size, int[][]map)
    {
        boolean  is_horiz = generator.nextInt(2) % 2 == 0;
        int   row_top;
        int   col_left;
        do{
            do{row_top = generator.nextInt(EdgeLen) % EdgeLen;}
            while(!is_horiz && row_top > EdgeLen - size);

            do{col_left = generator.nextInt(EdgeLen) % EdgeLen;}
            while(is_horiz && col_left > EdgeLen - size);}

        while(!ship_is_good(size, is_horiz, row_top, col_left, map));

        if(is_horiz){
            for(int j = col_left; j < col_left + size; ++j)
                map[row_top][j] = 1;
        }
        else{//вертикальный
            for(int i = row_top; i < row_top + size; ++i)
                map[i][col_left] = 1;
        }
    }

    public void ships(int [][]map){
        for(int i = 0; i < 1; ++i)
            set_ship_with_size(4, map);
        for(int i = 0; i < 2; ++i)
            set_ship_with_size(3, map);
        for(int i = 0; i < 3; ++i)
            set_ship_with_size(2, map);
        for(int i = 0; i < 4; ++i)
            set_ship_with_size(1, map);
    }

    public void printMaps() {
        System.out.println( "\n--- AI MAP ---" );
        System.out.println( "--------------" );
        System.out.println( "    1 2 3 4 5 6 7 8 9 10");
        for( int i = 0; i < EdgeLen; i++ ){
            System.out.print( "  " + ( i + 1 ) + "|");
            for( int j = 0; j < EdgeLen; j++ ) hidedPrint( mapAi, i, j);
            System.out.println();
        }
        System.out.println( "\n--- MY MAP ---" );
        System.out.println( "--------------" );
        System.out.println( "    1 2 3 4 5 6 7 8 9 10");
        for( int i = 0; i < EdgeLen; i++ ){
            System.out.print( "  " + ( i + 1 ) + "|" );
            for( int j = 0; j < EdgeLen; j++ ) showedPrint( mapMy, i, j);
            System.out.println();
        }
    }

    public void hidedPrint( int map[][], int i, int j ) {

        if( map[i][j] == -1 ) {
            System.out.print( "o|" );
        }
        else if( map[i][j] == 2 ) {
            System.out.print( "x|" );
        }
        else {
            System.out.print( "_|" );
        }
    }

    public void showedPrint( int map[][], int i, int j ) {

        if( map[i][j] == -1 ) {
            System.out.print( "o|" );
        }
        else if( map[i][j] == 2 ) {
            System.out.print( "x|" );
        }
        else if( map[i][j] == 1 ) {
            System.out.print( "<|" );
        }
        else {
            System.out.print( "_|" );
        }
    }

    public void shootAi() {
        boolean shootDone = false;
        while(!shootDone) {
            int max = 0;
            int x = 0;
            int y = 0;
            minMax( draftMap );
            for( int i = 0; i < EdgeLen; i++ ) {
                for( int j = 0; j < EdgeLen; j++ ) {
                    if( max < minMaxMap[i][j] ) {
                        max = minMaxMap[i][j];
                        x = i;
                        y = j;
                    }
                }
            }
            if( mapMy[x][y] == 2 || mapMy[x][y] == -1 ) {
                continue;
            }
            else {
                if( mapMy[x][y] == 1) {
                    mapMy[x][y] = 2;
                    draftMap[x][y] = 3;
                    countMyShips--;
                    shootDone = true;

                    System.out.println("--- AI HIT ---");
                    System.out.println("--------------");
                }
                else {
                    mapMy[x][y] = -1;
                    draftMap[x][y] = 0;
                    shootDone = true;
                    System.out.println("- AI MISSED --");
                    System.out.println("--------------");
                }
            }

            resetMinMaxMap();
        }
    }

    public void shootMy(){

        boolean shootDone = false;
        int x, y;
        System.out.println( "\nMake your shoot..." );

        while( !shootDone ) {
            x = in.nextInt()-1;
            y = in.nextInt()-1;

            if( x < EdgeLen && x >= 0 && y < EdgeLen && y >= 0 ) {
                if( mapAi[x][y] == 2 || mapAi[x][y] == -1 ) {
                    System.out.println( "You have already shoot there\nShoot another coordinates...." );
                }
                else {
                    if( mapAi[x][y] == 1) {
                        mapAi[x][y] = 2;
                        countAiShips--;
                        shootDone = true;
                        System.out.println("--------------");
                        System.out.println("-- YOU HIT ---");
                        System.out.println("--------------");
                    }
                    else {
                        mapAi[x][y] = -1;
                        System.out.println("--------------");
                        System.out.println("- YOU MISSED -");
                        System.out.println("--------------");
                        shootDone = true;
                    }
                }
            }
            else {
                System.out.println( "Wrong coordinates\nTry again...." );
            }
        }
    }

    public boolean gameOver() {
        if( countAiShips == 0 ){
            System.out.println("--------------");
            System.out.println("-- YOU WIN ---");
            System.out.println("--------------");
            return true;
        }
        else if( countMyShips == 0 ) {
            System.out.println("--------------");
            System.out.println("- YOU LOOSE --");
            System.out.println("--------------");
            return true;
        }
        else{
            return false;
        }
    }

    /** MINIMAX ALGORITHM **/

    public void minMax( int map[][] ) {

        for( int i = 0; i < EdgeLen; i++ ) {

            for( int j = 0; j < EdgeLen; j++ ) {

                countMinMax( map, i, j );
            }
        }
    }

    /** COUNT MINIMAX **/
//Counts the optimal shoot

    public void countMinMax( int map[][], int x, int y ) {

        int sum = 0;
        boolean hasHitVlue = false;

        if( ( x + 1 ) < EdgeLen && map[x][y] == 1 && map[x + 1][y] != 0 ) {

            if( map[x + 1][y] >= EdgeLen ) {
                hasHitVlue = true;
            }
            sum++;

        }
        if( ( x - 1 ) >= 0 && map[x][y] == 1 && map[x - 1][y] != 0) {

            if( map[x - 1][y] >= EdgeLen ) {
                hasHitVlue = true;
            }

            sum++;
        }

        if( ( y + 1 ) < EdgeLen && map[x][y] == 1 && map[x][y + 1] != 0) {

            if( map[x][y + 1] >= EdgeLen ) {
                hasHitVlue = true;
            }

            sum++;
        }
        if( ( y - 1 ) >= 0 && map[x][y] == 1 && map[x][y - 1] != 0) {

            if( map[x][y - 1] >= EdgeLen ) {
                hasHitVlue = true;
            }

            sum++;
        }

        if( hasHitVlue ) {
            minMaxMap[x][y] = 5;
        }
        else {
            minMaxMap[x][y] = sum;
        }
    }

    /** RESET MINIMAX MAP **/

    public void resetMinMaxMap() {
        for( int i = 0; i < EdgeLen; i++ ) {
            for( int j = 0; j < EdgeLen; j++ )
                minMaxMap[i][j] = draftMap[i][j];
        }
    }

    public void runGame() {
        fillMap();
        ships(mapMy);
        ships(mapAi);
        printMaps();

        while( !gameOver() ) {
            shootMy();
            shootAi();
            printMaps();
        }
    }

    public static void main( String [] args ) {
        battle battleship = new battle();
        battleship.runGame();
    }
}